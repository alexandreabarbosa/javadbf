import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ForkJoinPool;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import br.com.aab.datasusingestion.recursivetask.JavaDBFRecursiveTask;

public class JdbfMain {
	
	static String gdsString = "KONTR,C,1,0|N_MDP,C,8,0|W_LIST_NO,N,2,0|G32,N,3,0|N_RECEIVER,N,1,0|G33,C,10,0|G312,C,250,0|G35,N,13,2|G311,C,9,0|G318,C,14,0|G315,N,11,2|G317C,C,3,0|G221,C,3,0|G221_BUK,C,3,0|G42,N,15,2|KODS_PT,C,3,0|KODS_ABC2,C,2,0|N_TTH,C,30,0|G442REGNU,C,28,0|DELIV_PPP,C,6,0|G40T,C,2,0|G40,C,35,0|G405,N,2,0|TOV_SIGN2,C,1,0|CREATEDATE,D,8,0|MODIFIED_D,D,8,0|ARM_ID,N,3,0|VERSION,C,4,0";

	final static Logger logger = Logger.getLogger(JdbfMain.class);
	
	private String DATASUSENV = "";
	private Map<String, Path> hashMapPathDBF2Tables = new HashMap<String, Path>();
	private String jdbcDriver = "";
	private String jdbcDBConnection = "";
	private String jdbcUserName = "";
	private String jdbcPassWord = "";
	protected Double howMuchCPUs;
	protected List<String> listStringDBFRecords; 
	protected String strTableName = null;
	protected String strFileName = null;
	
	public static void main(String[] args) throws Exception {
		
		JdbfMain jdbfMain = new JdbfMain();
		jdbfMain.beginIngest();

	}

	public JdbfMain() { }

	public JdbfMain(List<String> listStringDBFRecords, String strTableName, String strFileName) {
		this.listStringDBFRecords = listStringDBFRecords;
		this.strTableName = strTableName;
		this.strFileName = strFileName;
	}
	
	private void beginIngest() {
		String strLog4JConfigure = "";
		DATASUSENV = System.getProperty("user.name");
		if ("alexandre1202".equalsIgnoreCase(DATASUSENV)) {
			strLog4JConfigure = "/Users/alexandre1202/Documents/optum/datasus/log4j.properties";
//			hashMapPathDBF2Tables.put("siasuspa", Paths.get("/Users/alexandre1202/Documents/optum/datasus/dbf_pasp/"));
			hashMapPathDBF2Tables.put("sihsusrd", Paths.get("/Users/alexandre1202/Documents/optum/datasus/dbf_rdsp/"));
//			hashMapPathDBF2Tables.put("siasusab", Paths.get("/Volumes/AAB@2TB/datasus/dados/dbfs/siasus/ab/"));
//			hashMapPathDBF2Tables.put("sihsussp", Paths.get("/Volumes/AAB@2TB/datasus/dados/dbfs/sihsus/sp/"));
//			hashMapPathDBF2Tables.put("sihsusrd", Paths.get("/Volumes/AAB@2TB/datasus/dados/dbfs/sihsus/rd/"));
//			hashMapPathDBF2Tables.put("cnesst", Paths.get("/Volumes/AAB@2TB/datasus/dados/dbfs/cnes/st/"));
//			hashMapPathDBF2Tables.put("cadgerbr2", Paths.get("/Users/alexandre1202/Documents/optum/datasus/datasus.gov.br/tabelasAuxiliares/siasus/TAB_SIA/TAB_DBF/cadgerdbf"));
//			hashMapPathDBF2Tables.put("municipiosus", Paths.get("/Users/alexandre1202/Documents/optum/datasus/datasus.gov.br/Dados/base-territorial/municipiosus/"));

			jdbcDriver = "com.mysql.cj.jdbc.Driver";
			jdbcDBConnection  = "jdbc:mysql://localhost:3306/datasus?useSSL=false";
			jdbcUserName = "root";
			jdbcPassWord = "";
						
		} else if ("datasus".equalsIgnoreCase(DATASUSENV)) {
			strLog4JConfigure = "/home/datasus/javaapps/conf/log4j.properties";
			jdbcDriver = "oracle.jdbc.driver.OracleDriver";
			jdbcDBConnection  = "jdbc:oracle:thin:@localhost:1521:datasus";
			jdbcUserName = "datasus";
			jdbcPassWord = "sus2019!";

			//hashMapPathDBF2Tables.put("sihsussp", Paths.get("/mnt/usb-JMicron_JMicron_DB9876543213E-0:0-part2/datasus/dados/dbfs/sihsus/sp"));
			hashMapPathDBF2Tables.put("siasuspa", Paths.get("/mnt/usb-JMicron_JMicron_DB9876543213E-0:0-part2/datasus/dados/dbfs/siasus/pa"));
						
		} else if ("desenv".equalsIgnoreCase(DATASUSENV)) {
			strLog4JConfigure = "/u01/datasus/app/log4j.properties";
			jdbcDriver = "oracle.jdbc.driver.OracleDriver";
			jdbcDBConnection  = "jdbc:oracle:thin:@localhost:1521:datasus";
			jdbcUserName = "datasus";
			jdbcPassWord = "optumsus";

			hashMapPathDBF2Tables.put("sihsusrd", Paths.get("/u01/datasus/dbf/sihsus/rd/"));
						
		} else { 
			System.out.println("Unknow enviroment!");
			System.exit(0);
		}
		String filenamelog = hashMapPathDBF2Tables.keySet().toString();
		filenamelog = filenamelog.substring(1,filenamelog.length());		
		filenamelog = filenamelog.substring(0,filenamelog.length()-1);
		System.setProperty("logfilename", filenamelog);
		PropertyConfigurator.configure(strLog4JConfigure);	
		howMuchCPUs = Double.valueOf(Runtime.getRuntime().availableProcessors()) * 0.85;
	
		ingestion();
	}
	
	public void ingestion() {
		
		List<Path> listPathDBFFile = new ArrayList<Path>();
		
		hashMapPathDBF2Tables.forEach((key, value) -> {
			try {
				Files.list(value.toAbsolutePath()).forEach(listPathDBFFile::add);
				logger.info("DATASUS has started - File list " + key + " has been loaded!");
				ForkJoinPool.commonPool().invoke( 
						new JavaDBFRecursiveTask(jdbcDriver, jdbcDBConnection, jdbcUserName, jdbcPassWord, 
							DATASUSENV, howMuchCPUs, listPathDBFFile, key) );

//				listPathDBFFile.forEach(dbfFileNamePath -> {										
//					List<String> listInsert = feedDataSus(key, new File(value+ "/" + dbfFileNamePath.getFileName()));
//					if (listInsert != null) {
//						ForkJoinPool.commonPool().invoke( new JavaDBFRecursiveTask(jdbcDriver, jdbcDBConnection, jdbcUserName, jdbcPassWord, DATASUSENV, howMuchCPUs, listInsert, key, value+ "/" + dbfFileNamePath.getFileName()) );
//					}
//				});
			} catch (IOException ioException) {
				logger.error("Erro ao tentar obter a lista de arquivos para o diretorio - " + value + " - " + ioException.getMessage() + " - causa : " + ioException.getCause());
				ioException.printStackTrace();
			} 
		});

	}
	
	private Collection<JdbfMain> forkJoinDBFRecords(List<String> listInsert, String strTableName, String strFileName) {
		
		List<JdbfMain> listJdbfMain = new ArrayList<JdbfMain>(); 
		listJdbfMain.add(new JdbfMain(listInsert.subList(0, listInsert.size() / 2), strTableName, strFileName));
		listJdbfMain.add(new JdbfMain(listInsert.subList(listInsert.size() / 2, listInsert.size()), strTableName, strFileName));
		
		return listJdbfMain;
	
	}

}
