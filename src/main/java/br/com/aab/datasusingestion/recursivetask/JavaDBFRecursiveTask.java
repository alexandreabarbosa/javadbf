package br.com.aab.datasusingestion.recursivetask;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.RecursiveTask;
import java.util.concurrent.atomic.AtomicLong;

import org.apache.log4j.Logger;

import br.com.aab.datasusingestion.service.ConvertDBFToInsertTable;

public class JavaDBFRecursiveTask extends RecursiveTask<Long> {

	private String DATASUSENV;
	private Double howMuchCPUs;
	private List<Path> listPathDBF;
	private String strTableName;
	private String jdbcDriver;
	private String jdbcDBConnection;
	private String jdbcUserName;
	private String jdbcPassWord;
	final static Logger logger = Logger.getLogger(JavaDBFRecursiveTask.class);
	
	public JavaDBFRecursiveTask(String jdbcDriver, String jdbcDBConnection, String jdbcUserName, String jdbcPassWord,String DATASUSENV, 
				Double howMuchCPUs, 
				List<Path> listPathDBF, String strTableName) {
		super();
		this.DATASUSENV = DATASUSENV;
		this.howMuchCPUs = howMuchCPUs;
		this.listPathDBF = listPathDBF;
		this.strTableName = strTableName;
		this.jdbcDriver = jdbcDriver;
		this.jdbcDBConnection = jdbcDBConnection;
		this.jdbcUserName = jdbcUserName;
		this.jdbcPassWord = jdbcPassWord;
		System.setProperty("logfilename", strTableName);		
	}
	
	@Override
	protected Long compute() {

		if (this.listPathDBF.size() > howMuchCPUs) {
			logger.info("Forking successfully with [" +  this.listPathDBF.size() + "] files!");
			return ForkJoinTask.invokeAll(forkJoinDBFRecords(this.listPathDBF, this.strTableName))
					.parallelStream()
					.mapToLong(ForkJoinTask::join)
					.sum();
		} else {
			return commitDataSUS(this.listPathDBF, this.strTableName);
		}
	}
	
	private Collection<JavaDBFRecursiveTask> forkJoinDBFRecords(List<Path> listPathDBF, String strTableName) {
		
		List<JavaDBFRecursiveTask> listJdbfMain = new ArrayList<JavaDBFRecursiveTask>(); 
		listJdbfMain.add(new JavaDBFRecursiveTask(this.jdbcDriver, this.jdbcDBConnection, this.jdbcUserName, this.jdbcPassWord, this.DATASUSENV, 
				this.howMuchCPUs, 
				listPathDBF.subList(0, listPathDBF.size() / 2), strTableName));
		listJdbfMain.add(new JavaDBFRecursiveTask(this.jdbcDriver, this.jdbcDBConnection, this.jdbcUserName, this.jdbcPassWord, this.DATASUSENV, 
				this.howMuchCPUs, 
				listPathDBF.subList(listPathDBF.size() / 2, listPathDBF.size()), strTableName));
		logger.info("Forking successfully with [" +  this.listPathDBF.size() + "] files!");
		return listJdbfMain;	
	}
	
	private Long commitDataSUS(List<Path> listPathDBF, String strTableName) {
		
		logger.info(this.listPathDBF.size() + " files to be commited in this Fork Parallel!");
		AtomicLong aLongRecordsInserted = new AtomicLong();
		listPathDBF.parallelStream().forEach(pathDBF -> {
			logger.info("Preparing to commit filename [" +  pathDBF.toString() + "] files!");	
			ConvertDBFToInsertTable convert = new ConvertDBFToInsertTable(this.DATASUSENV, pathDBF, strTableName, this.howMuchCPUs, 
					this.jdbcDriver, this.jdbcDBConnection, this.jdbcUserName, this.jdbcPassWord);
			aLongRecordsInserted.addAndGet(convert.feedDataSus());
		});
		
		return aLongRecordsInserted.get();
	}	
	 
}
