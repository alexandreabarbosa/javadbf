package br.com.aab.datasusingestion.service;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.Normalizer;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.concurrent.atomic.AtomicLong;

import org.apache.log4j.Logger;

import br.com.aab.javadbf.core.DbfMetadata;
import br.com.aab.javadbf.core.DbfRecord;
import br.com.aab.javadbf.reader.DbfReader;

public class ConvertDBFToInsertTable {
	
	final static Logger logger = Logger.getLogger(ConvertDBFToInsertTable.class);
	
	private String DATASUSENV;
	private String strTableName;
	private Path pathDBF;
	private Double howMuchCPUs;
	private String jdbcDriver;
	private String jdbcDBConnection;
	private String jdbcUserName;
	private String jdbcPassWord;
	
	public ConvertDBFToInsertTable(String DATASUSENV, Path pathDBF, String strTableName, Double howMuchCPUs,
			String jdbcDriver, String jdbcDBConnection, String jdbcUserName, String jdbcPassWord) {
		this.DATASUSENV = DATASUSENV;
		this.strTableName = strTableName;
		this.pathDBF = pathDBF;
		this.howMuchCPUs = howMuchCPUs;
		this.jdbcDriver = jdbcDriver;
		this.jdbcDBConnection = jdbcDBConnection;
		this.jdbcUserName = jdbcUserName;
		this .jdbcPassWord = jdbcPassWord;
		System.setProperty("logfilename", strTableName);				
	}
	
	public Long feedDataSus()  {

		List<String> listInsert = new ArrayList<String>();
		
		String strCharSet = "Cp866";
		strCharSet = "ISO-8859-1";
		Charset stringCharset = Charset.forName(strCharSet);
		
		DbfReader reader = null;
		File fileDBF = new File(pathDBF.toString());
		try {
			reader = new DbfReader(fileDBF);
		} catch (IOException e) {
			logger.error("Erro ao tentar ler o DBF (" + strTableName + ") - " + e.getMessage() + " - causa : " + e.getCause());
			e.printStackTrace();
		}
		DbfMetadata meta = reader.getMetadata();
		DbfRecord rec = null;
		AtomicLong aLongPreparingInserts = new AtomicLong();
		
		try {
			while ((rec = reader.read()) != null) {
				rec.setStringCharset(stringCharset);
				Map<String,Object> map = rec.toMap();
				listInsert.add((getInsertTableFields(map.toString(), strTableName)));
				
				if (aLongPreparingInserts.incrementAndGet() % 200000 == 0) {
					insertRecords(listInsert, aLongPreparingInserts);		
					listInsert = new ArrayList<String>();
				}
			}
			insertRecords(listInsert, aLongPreparingInserts);		
		} catch (ParseException parseException) {
			logger.error("Erro ao tentar ler o DBF - " + parseException.getMessage() + " - causa : " + parseException.getCause());
			parseException.printStackTrace();
		} catch (IOException ioException) {
			logger.error("Erro ao tentar ler o DBF - " + ioException.getMessage() + " - causa : " + ioException.getCause());
			ioException.printStackTrace();
		}
		logger.info("(" + this.howMuchCPUs + ") CPUs -> INSERT de " 
				+ String.format("%,d", aLongPreparingInserts.get()) 
				+ " registros do arquivo " + pathDBF + " na " + strTableName + " EFETUADO!");
		
		try {
			reader.close();
		} catch (IOException ioException) {
			logger.error("Erro ao tentar fechar o DBF (" + strTableName + ") - " + ioException.getMessage() + " - causa : " + ioException.getCause());
			ioException.printStackTrace();
		}	
		return aLongPreparingInserts.get();
	}

	private void insertRecords(List<String> listInsert, AtomicLong aLongPreparingInserts) {
		try {
			Statement statementDB = connectionDatabase();
			listInsert.parallelStream().forEach(line -> {
				try {
					statementDB.addBatch(line);
				} catch (SQLException e) {
					logger.error("Erro ao tentar inserir a linha " + line + "- ERRORCODE = " + e.getErrorCode() 
					+ " - ERRORMESSAGE = " + e.getMessage()
					+ " - CAUSA = " + e.getCause());
					e.printStackTrace();
				}
			});
			statementDB.executeBatch();
			statementDB.getConnection().commit();
			statementDB.getConnection().close();
			logger.info("DATASUS database has been closed successfully!");
		} catch (SQLException e) {
			logger.error("Erro ao tentar inserir - ERRORCODE = " + e.getErrorCode() 
					+ " - ERRORMESSAGE = " + e.getMessage()
					+ " - CAUSA = " + e.getCause());
			e.printStackTrace();
		}
	}	
	
    private String getInsertTableFields(String strFieldsName, String tableName) {
    	StringBuilder sBulderInsertTable = null;
		StringBuilder sBuilderValuesList = new StringBuilder();
		StringBuilder sBuilderNameList = new StringBuilder();
		strFieldsName = cleanDuplicatedComma(strFieldsName);
		StringTokenizer stzFields = new StringTokenizer(strFieldsName, ",");
		final String strQuotes = "alexandre1202".equals(this.DATASUSENV) ? "`" : "";
		StringBuilder sBuilderFieldName = new StringBuilder();
		StringBuilder sBuilderValueName = new StringBuilder();
		StringBuilder sBuilderField = new StringBuilder();
		while(stzFields.hasMoreElements()) {
			sBuilderField.append(stzFields.nextElement().toString());
			int intEqualPosition = sBuilderField.toString().indexOf("=");
			if (intEqualPosition > 0) {
				sBuilderFieldName.append((sBuilderField.substring(0, intEqualPosition)));
				sBuilderValueName.append(sBuilderField.substring(intEqualPosition + 1, sBuilderField.length()));

				sBuilderNameList.append(strQuotes);
				sBuilderNameList.append(sBuilderFieldName.toString().trim());
				sBuilderNameList.append(strQuotes);
				sBuilderNameList.append(",");
				
				sBuilderValuesList.append("'");
				if ( ( !sBuilderValueName.toString().equalsIgnoreCase("null") ) && ( !sBuilderValueName.toString().contains("'") ) ) {
					sBuilderValuesList.append(Normalizer.normalize(sBuilderValueName.toString(), Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", ""));
				}
				sBuilderValuesList.append("',");
			} else {
				logger.info("Problema com a coluna [" + sBuilderField + "] do arquivo [" + tableName  + "] pois nao contem o sinal de EQUAL" );
			}
			sBuilderField.delete(0, sBuilderField.length());
			sBuilderValueName.delete(0, sBuilderValueName.length());
			sBuilderFieldName.delete(0, sBuilderFieldName.length());
		}
		sBulderInsertTable = new StringBuilder();
		sBulderInsertTable.append("INSERT INTO datasus.").append(tableName)
			.append("(").append(sBuilderNameList.toString().substring(0, sBuilderNameList.toString().length() - 1))
			.append(") VALUES (")
			.append(sBuilderValuesList.toString().substring(0, sBuilderValuesList.toString().length() - 1))
			.append(")");
		
		return sBulderInsertTable.toString();
    }
    
	private String cleanDuplicatedComma(String line) {
		boolean hasAlreadyComma = false;
		StringBuilder sBuilderLine = new StringBuilder();
		StringBuilder sBuilderChar = new StringBuilder();
		for (int i = line.length(); i > 0; i--) {
			sBuilderChar.append(line.substring(i-1, i));
			if ( ",".equals(sBuilderChar.toString()) ) {
				if ( hasAlreadyComma ) {
					sBuilderLine.append(" ");
				} else {
					sBuilderLine.append(sBuilderChar);
				}
				hasAlreadyComma = true;
			} else if ("=".equals(sBuilderChar.toString())) {
				sBuilderLine.append(sBuilderChar.toString());
				hasAlreadyComma = false;
			} else if ("{".equals(sBuilderChar.toString().trim()) 
					|| "}".equals(sBuilderChar.toString().trim() ) 
					|| "'".equals(sBuilderChar.toString()) 
					|| ("\\").equals(sBuilderChar.toString())) {
			} else {
				sBuilderLine.append(sBuilderChar.toString());
			}
			sBuilderChar.delete(0, sBuilderChar.length());
		}
		
		return sBuilderLine.reverse().toString();
	}

	private Statement connectionDatabase() {
		
		Statement statementDB = null;

		try {
			Class.forName(this.jdbcDriver).newInstance();
			Connection connectionDB = DriverManager.getConnection(this.jdbcDBConnection, this.jdbcUserName, this.jdbcPassWord);	
			connectionDB.setAutoCommit(false);
			statementDB = connectionDB.createStatement();
		} catch (InstantiationException e) {
			logger.error("Erro ao instanciar o driver JDBC - ERROR MESSAGE: " + e.getMessage()
			+ " - CAUSE : "  + e.getCause()
			+ " - EXCEPTION : " + e.getStackTrace());
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			logger.error("Erro ao acessar o driver JDBC - ERROR MESSAGE: " + e.getMessage()
			+ " - CAUSE : "  + e.getCause()
			+ " - EXCEPTION : " + e.getStackTrace());
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			logger.error("Erro ao carregar o driver JDBC - ERROR MESSAGE: " + e.getMessage()
					+ " - CAUSE : "  + e.getCause()
					+ " - EXCEPTION : " + e.getException());
			e.printStackTrace();
		} catch (SQLException e) {
			logger.error("Erro ao carregar o driver JDBC - ERROR MESSAGE: " + e.getMessage()
			+ " - CAUSE : "  + e.getCause()
			+ " - ERROR CODE : " + e.getErrorCode());
			e.printStackTrace();
		}
		logger.info("DATASUS database has connected successfully!");

		return statementDB;
	}	
	
}
