<project xmlns="http://maven.apache.org/POM/4.0.0"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>

	<groupId>br.com.aab</groupId>
	<artifactId>javadbf</artifactId>
	<version>1.0</version>
	<packaging>jar</packaging>

	<properties>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		<maven.compiler.source>1.8</maven.compiler.source>
		<maven.compiler.target>1.8</maven.compiler.target>
		<javadoc.extra.settings />
		<maven.compiler.version>3.3</maven.compiler.version>
		<junit.version>4.12</junit.version>
	</properties>

	<dependencies>

		<dependency>
			<groupId>com.oracle</groupId>
			<artifactId>ojdbc8</artifactId>
			<version>12.2.0.1</version>
		</dependency>

		<dependency>
			<groupId>log4j</groupId>
			<artifactId>log4j</artifactId>
			<version>1.2.17</version>
		</dependency>

		<dependency>
			<groupId>junit</groupId>
			<artifactId>junit</artifactId>
			<version>${junit.version}</version>
			<scope>test</scope>
		</dependency>
		<!-- https://mvnrepository.com/artifact/mysql/mysql-connector-java -->
		<dependency>
			<groupId>mysql</groupId>
			<artifactId>mysql-connector-java</artifactId>
			<version>8.0.13</version>
		</dependency>
	</dependencies>

	<build>
		<pluginManagement>
			<plugins>
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-release-plugin</artifactId>
					<version>2.5.3</version>
					<configuration>
						<autoVersionSubmodules>true</autoVersionSubmodules>
						<preparationGoals>clean install</preparationGoals>
						<goals>deploy</goals>

						<tagNameFormat>installcert-@{project.version}</tagNameFormat>
					</configuration>
				</plugin>
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-javadoc-plugin</artifactId>
					<version>2.10.3</version>
					<configuration>
						<aggregate>true</aggregate>
						<additionalparam>${javadoc.extra.settings}</additionalparam>
					</configuration>
					<executions>
						<execution>
							<id>aggregate</id>
							<goals>
								<goal>aggregate</goal>
							</goals>
							<phase>site</phase>
						</execution>
					</executions>
				</plugin>
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-deploy-plugin</artifactId>
					<version>2.8.2</version>
				</plugin>
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-release-plugin</artifactId>
					<version>2.5.3</version>
					<configuration>
						<autoVersionSubmodules>true</autoVersionSubmodules>
						<preparationGoals>clean install</preparationGoals>
						<goals>deploy</goals>
						<releaseProfiles>release</releaseProfiles>
						<tagNameFormat>jdbf-@{project.version}</tagNameFormat>
					</configuration>
				</plugin>

				<plugin>
					<artifactId>maven-assembly-plugin</artifactId>
					<configuration>
						<archive>
							<manifest>
								<mainClass>JdbfMain</mainClass>
								<attachToBuild>true</attachToBuild>
							</manifest>
						</archive>
						<descriptorRefs>
							<descriptorRef>jar-with-dependencies</descriptorRef>
						</descriptorRefs>
					</configuration>
					<executions>
						<execution>
							<id>make-assembly</id> <!-- this is used for inheritance merges -->
							<phase>package</phase> <!-- bind to the packaging phase -->
							<goals>
								<goal>single</goal>
							</goals>
						</execution>
					</executions>
				</plugin>

			</plugins>
		</pluginManagement>
		<plugins>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-compiler-plugin</artifactId>
				<version>${maven.compiler.version}</version>
			</plugin>

			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-surefire-plugin</artifactId>
				<version>3.0.0-M3</version>
				<configuration>
					<skipTests>true</skipTests>
				</configuration>
			</plugin>

			<plugin>
				<!-- Build an executable JAR -->
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-jar-plugin</artifactId>
				<version>3.1.0</version>
				<configuration>
					<archive>
						<manifest>
							<addClasspath>true</addClasspath>
							<classpathPrefix>libs/</classpathPrefix>
							<mainClass>JdbfMain</mainClass>
						</manifest>
					</archive>
				</configuration>
			</plugin>

		</plugins>
	</build>

	<profiles>
		<profile>
			<id>doclint-java8-disable</id>
			<activation>
				<jdk>[1.8,)</jdk>
			</activation>
			<properties>
				<javadoc.extra.settings>-Xdoclint:none</javadoc.extra.settings>
			</properties>
		</profile>

		<!-- START SNIPPET: release-profile -->
		<profile>
			<id>release</id>
			<build>
				<plugins>
					<plugin>
						<inherited>true</inherited>
						<groupId>org.apache.maven.plugins</groupId>
						<artifactId>maven-deploy-plugin</artifactId>
						<configuration>
							<updateReleaseInfo>true</updateReleaseInfo>
						</configuration>
					</plugin>
					<plugin>
						<groupId>org.apache.maven.plugins</groupId>
						<artifactId>maven-source-plugin</artifactId>
						<executions>
							<execution>
								<id>attach-sources</id>
								<goals>
									<goal>jar-no-fork</goal>
								</goals>
							</execution>
						</executions>
					</plugin>

					<plugin>
						<groupId>org.apache.maven.plugins</groupId>
						<artifactId>maven-javadoc-plugin</artifactId>
					</plugin>
					<!-- We want to sign the artifact, the POM, and all attached artifacts -->
					<plugin>
						<groupId>org.apache.maven.plugins</groupId>
						<artifactId>maven-gpg-plugin</artifactId>
						<version>1.6</version>
						<executions>
							<execution>
								<id>sign-release-artifacts</id>
								<goals>
									<goal>sign</goal>
								</goals>
							</execution>
						</executions>
					</plugin>
				</plugins>
			</build>
		</profile>
	</profiles>

</project>
